## Intro
* [@joshunger](https://twitter.com/joshunger) / joshunger1@gmail.com
* Software Engineer at [IdeaRoom](http://www.idearoominc.com) in Boise, Idaho
* Spend 10 minutes talking about the latest browser news
* If you know more about the news, stop me and tell us more
* Let's make this interactive

## News
* Chrome Dev Summit 11/10 and 11/11
  * Playlist - https://www.youtube.com/watch?v=tI2QCxbpsuI&list=PLNYkxOF6rcIBTs2KPy1E6tIYaWoFcG3uj
  * Highlights - https://www.youtube.com/watch?v=tI2QCxbpsuI
  * Debugging the Web - https://www.youtube.com/watch?v=HF1luRD4Qmk

## Google Chrome

* Chrome 55 (stable)
  * [async / await](https://developers.google.com/web/updates/2016/11/nic55#async-and-await)
  * [CSS Coverage in devtools](https://umaar.com/dev-tips/121-css-coverage/) - 55?
* Chrome 56 (beta)
  * [DevTools Console is powered by CodeMirror](https://developers.google.com/web/updates/2016/10/devtools-digest)
* Chrome 57 (canary) - https://www.chromestatus.com/features#milestone%3D57
  *  Shape Detection API
     * Mobile (201110), Apple (a52011)  
     * https://github.com/WICG/shape-detection-api
     * https://wicg.github.io/shape-detection-api/#examples
  * `text-decoration-skip: ink;`
     * https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-skip
     * https://medium.design/crafting-link-underlines-on-medium-7c03a9274f9#.xe3wyrdf3
  * `text-decoration: underline red`
     * https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration 
     
## FireFox
* [FireFox 52](https://developer.mozilla.org/en-US/Firefox/Releases/52) - March 2017
  * [es support at 91%!!!!!!!!](http://kangax.github.io/compat-table/es2016plus/)
  * [async/await support](https://blog.nightly.mozilla.org/2016/11/01/async-await-support-in-firefox/)
* [FireFox 53](https://developer.mozilla.org/en-US/Firefox/Releases/53)
  * April 2017... not many notes yet!
* FireFox Nightly
  * [Simulate slow connections with the network throttling tool](https://blog.nightly.mozilla.org/2016/11/07/simulate-slow-connections-with-the-network-throttling-tool/)

## Safari
* [Safari Technology Preview 17](https://webkit.org/blog/7071/release-notes-for-safari-technology-preview-17/)
* [Safari Technology Preview 18](https://webkit.org/blog/7078/release-notes-for-safari-technology-preview-18/)
* [Safari Technology Preview 19](https://webkit.org/blog/7093/release-notes-for-safari-technology-preview-19/)
  * Touch Bar!

## Does anyone have news on .... ?
* Microsoft Edge
* WebStorm
* angular
* react/redux
* es7
* webpack
* vue

## For weekend update, I'm Josh Unger  

## Future Reading
* Chrome
  * https://googlechromereleases.blogspot.com/
  * https://developers.google.com/web/updates/2016/
  * https://developers.google.com/web/updates/2016/10/devtools-digest
  * https://twitter.com/DevToolsCommits
  * https://blog.chromium.org/
  * https://www.chromestatus.com/features
* Internet Explorer
  * https://blogs.windows.com/msedgedev/
  * https://developer.microsoft.com/en-us/microsoft-edge/platform/changelog/insider/
* FireFox
  * https://www.mozilla.org/en-US/firefox/releases/
  * https://developer.mozilla.org/en-US/Firefox/Releases/
  * https://developer.mozilla.org/en-US/Firefox/Experimental_features
  * https://blog.nightly.mozilla.org/
  * https://twitter.com/FirefoxNightly

* http://stateofjs.com/

