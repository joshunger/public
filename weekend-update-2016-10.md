## Intro
* Josh Unger
* Software Engineer at IdeaRoom - http://www.idearoominc.com
* Expect some surveys and "Ask the audience" - quick 1 minute if you know more about it
* Yell out questions

## Politics
![](https://gitlab.com/joshunger/public/raw/master/400-pound-hacker-offended-by-trump.mp4)

## Google Chrome
* [Chrome 53 September 30th](https://googlechromereleases.blogspot.com/2016/09/stable-channel-updates-for-chrome-os.html)
  * Security updates with someone winning $100k
* [Chrome 53 Android Pay and `PaymentRequest` API](https://developers.google.com/web/fundamentals/getting-started/primers/payment-request/?hl=en)
* [BroadcastChannel API](https://developers.google.com/web/updates/2016/09/broadcastchannel)
  * Send messages to other windows/tabs of same-origin
* [Chrome 54 Deprecations](https://developers.google.com/web/updates/2016/09/chrome-54-deprecations)
* [Cross-origin service workers](https://developers.google.com/web/updates/2016/09/foreign-fetch)
  * Imagine, for instance, that you're an analytics provider. By deploying a foreign fetch service worker, you can ensure that all requests to your service that fail while a user is offline are queued and replayed once connectivity returns. 
* [CacheQueryOptions for `window.caches`](https://developers.google.com/web/updates/2016/09/cache-query-options)
* [DevTools - CPU throttling, network view, passive event listeners, group by activity, timeline stats in sources](https://developers.google.com/web/updates/2016/09/devtools-digest)

## Safari
* [Safari 10 was released September 20th](https://developer.apple.com/library/content/releasenotes/General/WhatsNewInSafari/Articles/Safari_10_0.html)
  * [Intl.NumberFormat Finally!](http://caniuse.com/#feat=internationalization)
  * [What's New](https://developer.apple.com/library/content/releasenotes/General/WhatsNewInSafari/Articles/Safari_10_0.html)
  * [Picture in Picture Demo](http://www.apple.com/apple-events/september-2016/)
  * [Apple Pay JS - `window.ApplePaySession` (open link in Safari)](https://developer.apple.com/videos/play/wwdc2016/703/)
     * See http://www.howtogeek.com/272664/how-to-shop-with-apple-pay-on-macos-sierra/

## FireFox
* [FireFox Nightly](https://twitter.com/FirefoxNightly)
  * [Oct 3 - drops plug-in support other than Flash, no Java or Silverlight](https://bugzilla.mozilla.org/show_bug.cgi?id=1269807)
  * [Cache-Control: immutable - no more 304s](http://bitsup.blogspot.co.uk/2016/05/cache-control-immutable.html)
  * [FireFox's debugger.html in react and redux](https://hacks.mozilla.org/2016/09/introducing-debugger-html/)
* [FireFox 49 on September 20](https://www.mozilla.org/en-US/firefox/49.0/releasenotes/)
  * HTML5 video / audio 1.25X speed
  * [Web Speech API (also in Chrome)](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API#Speech_synthesis)
* [FireFox 48 on August 2](https://www.mozilla.org/en-US/firefox/48.0/releasenotes/)
  * [Process separation!](https://blog.mozilla.org/futurereleases/2016/08/02/whats-next-for-multi-process-firefox/)

## Microsoft IE / Edge
* Next time... 
* [IE11 blocking Flash older than April 12, 2016](https://blogs.windows.com/msedgedev/2016/09/13/blocking-out-of-date-flash/)

## Angular
* ?

## React / Redux
* ?

## ES7
* ?

## Webpack
* [Some day 2.x will be released - v2.1.0-beta.25 (14 days ago)](https://github.com/webpack/webpack/tree/v2.1.0-beta.25)

## Vue
* [Vue 2 released September 30th](https://medium.com/the-vue-point/vue-2-0-is-here-ef1f26acf4b8#.1ljnpaof4)
  * Faster than angular and react!

## For weekend update, I'm Josh Unger  

## Future Reading
* https://developers.google.com/web/updates/
* https://blogs.windows.com/msedgedev/
* https://www.mozilla.org/en-US/firefox/releases/
* https://developer.mozilla.org/en-US/Firefox/Experimental_features
* https://blog.nightly.mozilla.org/2016/09/22/these-weeks-in-firefox-1/
* https://googlechromereleases.blogspot.com/
* http://stateofjs.com/

